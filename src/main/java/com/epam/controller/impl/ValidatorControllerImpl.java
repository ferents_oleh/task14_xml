package com.epam.controller.impl;

import com.epam.controller.ValidatorController;
import com.epam.validator.XmlValidator;

public class ValidatorControllerImpl implements ValidatorController {
    @Override
    public void printValidationResult() {
        String xml = "/home/nomorethrow/IdeaProjects/task14_xml/src/main/resources/orangery.xml";

        String xsd = "/home/nomorethrow/IdeaProjects/task14_xml/src/main/resources/orangery.xsd";

        boolean result = XmlValidator.validateAgainstXsd(xml, xsd);
        if (result) {
            System.out.println("XML is correct");
        } else {
            System.out.println("XML is incorrect");
        }
    }
}
