package com.epam.controller.impl;

import com.epam.controller.ParserController;
import com.epam.model.Flower;
import com.epam.service.OrangeryParser;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;

public class ParserControllerImpl implements ParserController {
    @Override
    public void printParsedXML() {
        OrangeryParser orangeryParser = new OrangeryParser();
        String filename = "/home/nomorethrow/IdeaProjects/task14_xml/src/main/resources/orangery.xml";
        File xmlDocument = Paths.get(filename).toFile();
        SAXParserFactory factory = SAXParserFactory.newInstance();
        try {
            SAXParser parser = factory.newSAXParser();
            parser.parse(xmlDocument, orangeryParser);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
        List<Flower> result = orangeryParser.getFlowers();
        result.forEach(System.out::println);
    }
}
