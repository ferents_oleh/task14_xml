package com.epam;

import com.epam.controller.impl.ParserControllerImpl;
import com.epam.controller.impl.ValidatorControllerImpl;
import com.epam.view.Menu;

public class Application {
    public static void main(String[] args) {
        new Menu(new ParserControllerImpl(), new ValidatorControllerImpl());
    }
}
